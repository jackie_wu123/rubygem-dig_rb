%global _empty_manifest_terminate_build 0
%global gem_name dig_rb
Name:		rubygem-dig_rb
Version:	1.0.1
Release:	1
Summary:	Array/Hash/Struct#dig backfill for ruby
License:	MIT
URL:		https://github.com/jrochkind/dig_rb
Source0:	https://rubygems.org/gems/dig_rb-1.0.1.gem
BuildArch:	noarch

BuildRequires:	ruby
BuildRequires:	ruby-devel
BuildRequires:	rubygems
BuildRequires:	rubygems-devel rsync
Provides:	rubygem-dig_rb

%description
Array/Hash/Struct#dig backfill for ruby

%package help
Summary:	Development documents and examples for dig_rb
Provides:	rubygem-dig_rb-doc
BuildArch: noarch

%description help
Array/Hash/Struct#dig backfill for ruby

%prep
%autosetup -n dig_rb-1.0.1
gem spec %{SOURCE0} -l --ruby > dig_rb.gemspec

%build
gem build dig_rb.gemspec
%gem_install

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* %{buildroot}%{gem_dir}/
rsync -a --exclude=".*" .%{gem_dir}/* %{buildroot}%{gem_dir}/
if [ -d .%{_bindir} ]; then
	mkdir -p %{buildroot}%{_bindir}
	cp -a .%{_bindir}/* %{buildroot}%{_bindir}/
fi
if [ -d ext ]; then
	mkdir -p %{buildroot}%{gem_extdir_mri}/%{gem_name}
	if [ -d .%{gem_extdir_mri}/%{gem_name} ]; then
		cp -a .%{gem_extdir_mri}/%{gem_name}/*.so %{buildroot}%{gem_extdir_mri}/%{gem_name}
	else
		cp -a .%{gem_extdir_mri}/*.so %{buildroot}%{gem_extdir_mri}/%{gem_name}
fi
	cp -a .%{gem_extdir_mri}/gem.build_complete %{buildroot}%{gem_extdir_mri}/
	rm -rf %{buildroot}%{gem_instdir}/ext/
fi
rm -rf %{buildroot}%{gem_instdir}/{.gitignore,.travis.yml}
pushd %{buildroot}
touch filelist.lst
if [ -d %{buildroot}%{_bindir} ]; then
	find .%{_bindir} -type f -printf "/%h/%f\n" >> filelist.lst
fi
popd
mv %{buildroot}/filelist.lst .

%files -n rubygem-dig_rb -f filelist.lst
%dir %{gem_instdir}
%{gem_instdir}/*
%exclude %{gem_cache}
%{gem_spec}

%files help
%{gem_docdir}/*

%changelog
* Mon Aug 02 2021 Ruby_Bot <Ruby_Bot@openeuler.org> - 1.0.1-1
- Package Spec generated
